import fs from "fs/promises";
import path from "path";
import log from "@ajar/marker";





toDo();
// Todo init function.
export default async function toDo() {

    try {
        const params = {
            ADD: "add",
            DELETE: "delete",
            SHOW: "show",
            FILTER: "filter",
            HELP: "help",
            CHECK: "check",
            RESET: "reset-noregrets"
        }
        // Get params from CLI and initialize arguments.
        const [command, ...args] = process.argv.slice(2);
        const outputFilePath = "DB.json";
        const dbPath = `./DB.json`;
        const encode = 'utf-8';

        // Create DB.json file if there isnt such.
        try {
            const dbExist = await fs.access(dbPath);

        } catch (err) {
            try {
                const writeNewDBjson = await fs.writeFile(dbPath, "");
                log.green("New DB.json file created");
                return toDo();

            } catch (err) {
                log.red(err);
                throw new Error("Erron in create new DB.json file");

            }
        }
        let DBjson = await fs.readFile(dbPath, encode);
        console.log(`For help use command: "help"`);

        // Checks if data base is empty.
        if (!DBjson) {
            const DBtemplate = {
                "notes": [

                ],
                "currentId": 1
            }
            writeFile(DBtemplate, dbPath);
        }
        DBjson = await fs.readFile(dbPath, encode);
        let DB = JSON.parse(DBjson);

        const id = Number(DB.currentId);
        const notesArr = DB.notes;


        switch (command) {
            case params.ADD:
                addItem(id, args, notesArr);
                updateId(DB, id);
                writeFile(DB, outputFilePath);
                showList(DB);
                break;
            case params.DELETE:
                deleteItem(args[0], DB);
                writeFile(DB, outputFilePath);
                showList(DB);
                break;
            case params.CHECK:
                checkItem(args[0], DB);
                writeFile(DB, outputFilePath);
                showList(DB);
                break;
            case params.SHOW:
                showList(DB);
                break;

            case params.HELP:
                showHelp();
                break;

            case params.FILTER:
                filterManager(DB, args[0], showList);
                break;

            case params.RESET:
                resetDb(DB);
                writeFile(DB, outputFilePath);
                break;

            default:
                console.log(`${command} is not legal command (try add,delete,check or help for more). `);
        }
    } catch (err) {
        log.red("Error", err);
        throw new Error("Error", err);
    }

}


// Reset database and counter.
function resetDb(db) {
    db.notes = [];
    db.currentId = 1;
    log.red("Data base cleaned");
}


// Render help instructions.
function showHelp() {
    log.green("To-Do list commands:");
    console.log(`Add post: add "note example"`);
    console.log(`Delete note: delete "note id"`);
    console.log(`Check item: check "item id" `);
    console.log(`Show list: show`);
    console.log(`Filter notes : filter "filter flag" ("todo","done","all")`);
    log.red(`Reset data base: reset-noregrets`);
}


// Increasse id counter.
function updateId(db, id) {
    const curId = id + 1;
    db.currentId = curId.toString();

}

// Delete item by id and return the appropriate message.
function deleteItem(id, db) {
    let found = false;
    db.notes = db.notes.filter((item) => {
        if (item.id === Number(id)) {
            found = true;
        }
        return item.id !== Number(id);
    })
    found ? log.green(`Note id : ${id} deleted.`) : log.red(`No such id : ${id}`);
}

// Filter manaher function to handle filters rendering.
function filterManager(db, filterFlag, showFunction) {
    const params = {
        ALL: "all",
        TODO: "todo",
        DONE: "done"
    }

    let show = {
        notes: []
    };
    switch (filterFlag) {
        case params.ALL:
            showFunction(db);
            break;
        case params.DONE:
            show.notes = db.notes.filter((note) => {
                return note.checked === true;
            })
            showFunction(show);
            break;

        case params.TODO:
            show.notes = db.notes.filter((note) => {
                return note.checked === false;
            })
            showFunction(show);
            break;
        default:
            console.log(`${filterFlag} is not legal filter. (try done,todo,all).`);
    }

}

// Add new note to database.
function addItem(id, body, notesArr) {

    const newNote = {
        "id": id,
        "body": body.join(" "),
        "checked": false
    }
    notesArr.push(newNote);
}

// Checks completed notes. 
function checkItem(id, db) {
    let found = false;
    db.notes = db.notes.map((note) => {
        if (note.id === Number(id)) {
            found = true;
            note.checked = !note.checked;
            return note;
        } else {
            return note;
        }
    });
    if (!found){
        log.red("No such id");
    }
}


// Render list from database.
function showList(DB) {
    log.magenta("My to-do list:");
    log.magenta("Done | Id | Text");


    DB.notes.forEach(element => {
        if (element.checked) {
            log.green(`✓ ${element.id} ${element.body}`);
        } else {
            log.blue(`☐ ${element.id} ${element.body}`);
        }
    });
}

// Write database to output file.
async function writeFile(db, path) {
    try {
        const write = await fs.writeFile(path, JSON.stringify(db));
    } catch (error) {
        log.red("Error in writing to data base", err);
        throw new Error("Error", error);
    }

}